# README #

Idea of this repository to try some of the features available with akka framework for high availability and high scalability.

### What is this repository for? ###

* Demo application using akka framework calling external service. A
* 0.0.1.SNAPSHOT

### How do I get set up? ###

* Clone the repository
* build and run application
```
mvn clean package
java -jar target/akka-demo-app-0.0.1-SNAPSHOT.jar
```

### Who do I talk to? ###

* Author himself