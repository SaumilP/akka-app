package org.sandcastle.apps.extension;

import akka.actor.Extension;
import akka.actor.Props;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class SpringExtension implements Extension {
    public ApplicationContext context;

    public void initialize(ApplicationContext context) {
        this.context = context;
    }

    public Props props(String actorBeanName) {
        return Props.create(SpringActorProducer.class, context, actorBeanName);
    }
}
