package org.sandcastle.apps.beans;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode
@ToString
@Data
public class Task {
    private final String payload;
    private final Integer locationIndex;
    private final Integer priority;

    public Task(String payload, Integer locationIndex, Integer priority) {
        this.payload = payload;
        this.locationIndex = locationIndex;
        this.priority = priority;
    }
}
