package org.sandcastle.apps.beans;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum Location {
    LOS_ANGELES(37.8267, -122.4233f),
    PHEONIX(33.4499, -112.0712f),
    NEW_DELHI(28.6353, 77.225f),
    KARACHI(24.946218, 67.005615f),
    LONDON(51.508530, -0.076132f),
    NEW_YORK(40.730610, -73.935242f),
    CAPE_TOWN(-33.918861, 18.423300f),
    DUBAI(25.276987, 55.296249f),
    JOHANNESBURG(-26.195246, 28.034088f),
    MUMBAI(18.943888, 72.835991f),
    SYDNEY(-33.865143, 151.209900f),
    AUCKLAND(-36.848461, 174.763336f);

    private final double longitude;
    private final double latitude;

    Location(double latitude, double longitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String toString() {
        return latitude + "," + longitude;
    }

    private static final List<Location> VALUES = Collections.unmodifiableList(Arrays.asList(values()));

    public static Location byOrdinalValue(int ordinalValue) {
        return VALUES.get(ordinalValue);
    }
}
