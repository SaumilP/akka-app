package org.sandcastle.apps.services;

import org.sandcastle.apps.beans.Location;
import org.sandcastle.apps.beans.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import tk.plogitech.darksky.api.jackson.DarkSkyJacksonClient;
import tk.plogitech.darksky.forecast.*;
import tk.plogitech.darksky.forecast.model.Forecast;

// Spring repository can be configured to keep use of spring features
@Repository
public class TaskRepository {
    private final Logger log = LoggerFactory.getLogger(TaskRepository.class);

    @Value("${dark.sky.api.secret}")
    private String secretKey;

    public Long createTask(final Task task) {
        final Location location = Location.byOrdinalValue(task.getLocationIndex());
        ForecastRequest request = new ForecastRequestBuilder()
                .key(new APIKey(secretKey))
                .location(
                        new GeoCoordinates(
                                new Longitude(location.getLongitude()),
                                new Latitude(location.getLatitude())
                        )
                )
                .build();

        DarkSkyJacksonClient client = new DarkSkyJacksonClient();
        try {
            Forecast forecast = client.forecast(request);
            log.info("{} Forecast: {}", location.name(), forecast.getCurrently().getTemperature());
        } catch (ForecastException ex) {
            log.error("Failed to retrieve weather forecast", ex);
        }

        return 1L;
    }
}
