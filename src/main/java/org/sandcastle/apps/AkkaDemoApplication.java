package org.sandcastle.apps;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.PoisonPill;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import org.sandcastle.apps.beans.Task;
import org.sandcastle.apps.extension.SpringExtension;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Random;
import java.util.stream.IntStream;

@Configuration
@EnableAutoConfiguration
@ComponentScan("org.sandcastle.apps.config")
public class AkkaDemoApplication {

    public static void main(String[] args) throws Exception {
        ApplicationContext context = new SpringApplicationBuilder()
                .bannerMode(Banner.Mode.OFF)
                .sources(AkkaDemoApplication.class)
                .run(args);

        ActorSystem system = context.getBean(ActorSystem.class);

        final LoggingAdapter log = Logging.getLogger(system, "DemoAkkaApp");
        log.info("Starting up...");

        SpringExtension extension = context.getBean(SpringExtension.class);

        // Spring Extension to create properties for named Actor
        ActorRef supervisor = system.actorOf(
                extension.props("supervisor").withMailbox("akka.priority-mailbox")
        );

        final int max_mailbox_priority = 99;
        final int max_city_locations = 12;
        IntStream.range(1, 100)
                .parallel()
                .forEach(i -> {
                    final Task task = new Task("payload " + i,
                            new Random().nextInt(max_city_locations),
                            new Random().nextInt(max_mailbox_priority)
                    );

                    supervisor.tell(task, null);
                });
        log.info("Tasks submitted to mailbox");

        // Poison Pill will be queued with priority 100 as Last Message ( check PriorityMailbox class )
        supervisor.tell(PoisonPill.getInstance(), null);

        while (!supervisor.isTerminated()) {
            Thread.sleep(5000);
        }

        log.info("Created {} entries", 100);
        log.info("Shutting down...");

        system.terminate();
    }
}
