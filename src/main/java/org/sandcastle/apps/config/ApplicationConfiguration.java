package org.sandcastle.apps.config;

import akka.actor.ActorSystem;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.sandcastle.apps.extension.SpringExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;

@Configuration
@Lazy
@ComponentScan(basePackages = {
        "org.sandcastle.apps.services",
        "org.sandcastle.apps.actors",
        "org.sandcastle.apps.extension"
})
@PropertySource(value = "classpath:application.properties")
public class ApplicationConfiguration {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private SpringExtension springExtension;

    @Bean
    public ActorSystem actorSystem() {

        ActorSystem system = ActorSystem.create("AkkaTaskProcessing", akkaConfiguration());

        // Initialize app context in spring
        springExtension.initialize(applicationContext);
        return system;
    }

    @Bean
    public Config akkaConfiguration() {
        return ConfigFactory.load();
    }
}
