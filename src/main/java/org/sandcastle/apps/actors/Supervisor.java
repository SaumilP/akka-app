package org.sandcastle.apps.actors;

import akka.actor.ActorRef;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.routing.ActorRefRoutee;
import akka.routing.Routee;
import akka.routing.Router;
import akka.routing.SmallestMailboxRoutingLogic;
import org.sandcastle.apps.beans.Task;
import org.sandcastle.apps.extension.SpringExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.IntStream;

@Component
@Scope("prototype")
public class Supervisor extends UntypedActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), Supervisor.class);

    @Autowired
    private SpringExtension springExtension;

    private Router router;

    @Override
    public void preStart() throws Exception {
        log.info("Starting up");

        List<Routee> routees = new CopyOnWriteArrayList<>();
        IntStream.range(0, 100).parallel().forEach(i -> {
            ActorRef actor = getContext().actorOf(springExtension.props("taskActor2"));
            getContext().watch(actor);
            routees.add(new ActorRefRoutee(actor));
        });
        router = new Router(new SmallestMailboxRoutingLogic(), routees);
        super.preStart();
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Task) {
            router.route(message, getSender());
        } else if (message instanceof Terminated) {
            // Read task actors if one failed
            router = router.removeRoutee(((Terminated) message).actor());
            ActorRef actor = getContext().actorOf(springExtension.props("taskActor2"));
            getContext().watch(actor);
            router = router.addRoutee(new ActorRefRoutee(actor));
        } else {
            log.error("Unable to handle message {}", message);
        }
    }

    @Override
    public void postStop() throws Exception {
        log.info("Shutting down");
        super.postStop();
    }
}
