package org.sandcastle.apps.actors;

import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import org.sandcastle.apps.beans.Task;
import org.sandcastle.apps.services.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class TaskActor extends UntypedAbstractActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), TaskActor.class);

    @Autowired
    private TaskRepository repository;

    @Override
    public void onReceive(Object message) throws Throwable {
        Long result = repository.createTask((Task) message);
        log.debug("Created Task {}", result);
    }
}
