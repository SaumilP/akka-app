package org.sandcastle.apps.actors;

import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import org.sandcastle.apps.beans.Location;
import org.sandcastle.apps.beans.Task;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import tk.plogitech.darksky.api.jackson.DarkSkyJacksonClient;
import tk.plogitech.darksky.forecast.*;
import tk.plogitech.darksky.forecast.model.Forecast;

@Component
@Scope("prototype")
public class TaskActor2 extends UntypedAbstractActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), TaskActor2.class);

    @Value("${dark.sky.api.secret}")
    private String secretKey;

    private Long createTask(final Task task) {
        final Location location = Location.byOrdinalValue(task.getLocationIndex());
        ForecastRequest request = new ForecastRequestBuilder()
                .key(new APIKey(secretKey))
                .location(
                        new GeoCoordinates(
                                new Longitude(location.getLongitude()),
                                new Latitude(location.getLatitude())
                        )
                )
                .build();

        DarkSkyJacksonClient client = new DarkSkyJacksonClient();
        try {
            Forecast forecast = client.forecast(request);
            log.info("{} Forecast: {}", location.name(), forecast.getCurrently().getTemperature());
        } catch (ForecastException ex) {
            log.error("Failed to retrieve weather forecast", ex);
        }

        return 1L;
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        Long result = createTask((Task) message);
        log.debug("Created Task {}", result);
    }
}
